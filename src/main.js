var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize

var flock
var maxTrace = 128
var numOfBoids = 32
var positionHistory = create2DArray(numOfBoids, maxTrace, [0, 0], true)

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)

  flock = new Flock()
  for (let i = 0; i < numOfBoids; i++) {
    let b = new Boid(width / 2, height / 2)
    flock.addBoid(b)
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)
  flock.run()

  for (var i = 0; i < flock.boids.length; i++) {
    positionHistory[i].push([flock.boids[i].position.x, flock.boids[i].position.y])
    if (positionHistory[i].length > maxTrace) {
      positionHistory[i] = positionHistory[i].splice(1)
    }
  }

  for (var i = 0; i < positionHistory.length; i++) {
    for (var j = 0; j < positionHistory[i].length; j++) {
      fill(128 + 128 * (j / maxTrace) * sin(i * 0.1 + j * 0.1))
      noStroke()
      push()
      translate(positionHistory[i][j][0], positionHistory[i][j][1])
      ellipse(0, 0, 0.025 * boardSize)
      pop()
    }
  }
}

function Flock() {
  this.boids = []
}

Flock.prototype.run = function() {
  for (let i = 0; i < this.boids.length; i++) {
    this.boids[i].run(this.boids)
  }
}

Flock.prototype.addBoid = function(b) {
  this.boids.push(b)
}

function Boid(x, y) {
  this.acceleration = createVector(0, 0)
  this.velocity = createVector(random(-1, 1), random(-1, 1))
  this.position = createVector(x, y)
  this.r = 3.0
  this.maxspeed = 3
  this.maxforce = 0.25
}

Boid.prototype.run = function(boids) {
  this.flock(boids)
  this.update()
  this.borders()
}

Boid.prototype.applyForce = function(force) {
  this.acceleration.add(force)
}

Boid.prototype.flock = function(boids) {
  let sep = this.separate(boids)
  let ali = this.align(boids)
  let coh = this.cohesion(boids)
  sep.mult(1.5)
  ali.mult(1.0)
  coh.mult(1.0)
  this.applyForce(sep)
  this.applyForce(ali)
  this.applyForce(coh)
}

Boid.prototype.update = function() {
  this.velocity.add(this.acceleration)
  this.velocity.limit(this.maxspeed)
  this.position.add(this.velocity)
  this.acceleration.mult(0)
}

Boid.prototype.seek = function(target) {
  let desired = p5.Vector.sub(target, this.position)
  desired.normalize()
  desired.mult(this.maxspeed)
  let steer = p5.Vector.sub(desired, this.velocity)
  steer.limit(this.maxforce)
  return steer
}

Boid.prototype.borders = function() {
  if (dist(windowWidth * 0.5, windowHeight * 0.5, this.position.x, this.position.y) >= boardSize * 0.45) {
    var randDir = Math.random() * Math.PI * 2
    var randDist = Math.random() * boardSize * 0.45
    this.position.x = windowWidth * 0.5 + sin(randDir) * randDist
    this.position.y = windowHeight * 0.5 + cos(randDir) * randDist
  }
}

Boid.prototype.separate = function(boids) {
  let desiredseparation = 200.0
  let steer = createVector(0, 0)
  let count = 0
  for (let i = 0; i < boids.length; i++) {
    let d = p5.Vector.dist(this.position, boids[i].position)
    if ((d > 0) && (d < desiredseparation)) {
      let diff = p5.Vector.sub(this.position, boids[i].position)
      diff.normalize()
      diff.div(d)
      steer.add(diff)
      count++
    }
  }
  if (count > 0) {
    steer.div(count)
  }

  if (steer.mag() > 0) {
    steer.normalize()
    steer.mult(this.maxspeed)
    steer.sub(this.velocity)
    steer.limit(this.maxforce)
  }
  return steer
}

Boid.prototype.align = function(boids) {
  let neighbordist = 50
  let sum = createVector(0, 0)
  let count = 0
  for (let i = 0; i < boids.length; i++) {
    let d = p5.Vector.dist(this.position, boids[i].position);
    if ((d > 0) && (d < neighbordist)) {
      sum.add(boids[i].velocity)
      count++
    }
  }
  if (count > 0) {
    sum.div(count)
    sum.normalize()
    sum.mult(this.maxspeed)
    let steer = p5.Vector.sub(sum, this.velocity)
    steer.limit(this.maxforce)
    return steer
  } else {
    return createVector(0, 0)
  }
}

Boid.prototype.cohesion = function(boids) {
  let neighbordist = 200
  let sum = createVector(0, 0)
  let count = 0
  for (let i = 0; i < boids.length; i++) {
    let d = p5.Vector.dist(this.position, boids[i].position);
    if ((d > 0) && (d < neighbordist)) {
      sum.add(boids[i].position)
      count++
    }
  }
  if (count > 0) {
    sum.div(count)
    return this.seek(sum)
  } else {
    return createVector(0, 0)
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
